use toy_rsa::*;

#[test]
fn test_genkey() {
    let (p, q) = genkey();

    assert!(lcm(p, q) > EXP);
    assert!(toy_rsa_lib::gcd(EXP, lcm(p, q)) == 1);
}

#[test]
fn test_encrypt() {
    let p = 2876066593 as u64;
    let q = 2482928117 as u64;
    let public_key = p * q;
    let encrypted = encrypt(public_key, 5);
    assert_eq!(encrypted, 2965760286246547823)
}

#[test]
fn test_decrypt() {
    let key = (2876066593, 2482928117);
    let msg = 2965760286246547823;
    let decrypted = decrypt(key, msg);
    assert_eq!(decrypted, 5);
}