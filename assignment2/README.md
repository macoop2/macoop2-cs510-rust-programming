# Name
Matthew Cooper

# Project
rsa

# What I did
I implemented a library in Rust that uses RSA to encrypt and decrypt an integer. Also, it includes a public method to generate the key pair.

To test my work I created 3 unit tests.
1. Test GenKey
2. Test Encrypt
3. Test Decrypt

# How it went
This was fairly straight forward since a lot of the difficult functions were given to us using the toy-rsa cargo library. I tried creating namespaces but I don't know Rust's full story with namespaces yet.