
/// Fixed RSA encryption exponent.
pub const EXP: u64 = 65_537;

pub fn lcm(p: u32, q: u32) -> u64 {
    let p = p as u64;
    let q = q as u64;
    toy_rsa_lib::lcm(p - 1, q - 1)
}

/// Generate a pair of primes in the range `2**31..2**32`
/// suitable for RSA encryption with exponent.
pub fn genkey() -> (u32, u32) {
    loop {
        let p = toy_rsa_lib::rsa_prime();
        let q = toy_rsa_lib::rsa_prime();
        if lcm(p, q) > EXP && toy_rsa_lib::gcd(EXP, lcm(p, q)) == 1 {
            return (p, q);
        }
    }
}

/// Encrypt the plaintext `msg` using the RSA public `key`
/// and return the ciphertext.
pub fn encrypt(key: u64, msg: u32) -> u64 {
    toy_rsa_lib::modexp(msg as u64, EXP, key)
}

/// Decrypt the cipertext `msg` using the RSA private `key`
/// and return the resulting plaintext.
pub fn decrypt(key: (u32, u32), msg: u64) -> u32 {
    let (p, q) = key;
    let d = toy_rsa_lib::modinverse(EXP, lcm(p, q));
    toy_rsa_lib::modexp(msg, d, p as u64 * q as u64) as u32
}