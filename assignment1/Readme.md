#Name
Matthew Cooper

#Project
modexp

#What I did
I implemented a program that finds the mod exp given 3 numbers. modexp(x,y,m) = x^y mod m. The algorithm says to fail if (m-1)^2 will overflow, however, we were instructed to convert u64 to u128 type to not have to worry about overflow. If there is still an overflow all the exponent calculations are checked anyway.

#How it went
It went well. Instead, of using Panic, I prefer to use Result<T,E> where possible. I'm still a little confused on the strings
so I need to review that. Also, I need to get better at reading the function documentations so I don't need examples to assist.