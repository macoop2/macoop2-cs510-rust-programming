//! Command-line modular exponentiation tool
//!
//! Matthew Cooper 2023

use num_traits::checked_pow;
use std::env;
use std::str::FromStr;

//! This is the main function. It will look for 3 arguments from the command line that should be numbers.
//! The 3 numbers are passed to the modexp function.
//! Then we print the result of the modexp function.
fn main() {
    let mut numbers = Vec::new();

    for arg in env::args().skip(1) {
        numbers.push(u64::from_str(&arg).expect("error parsing argument"));
    }

    if numbers.len() != 3 {
        eprintln!("Usage: enter 3 numbers");
        std::process::exit(1);
    }

    let x = numbers[0];
    let y = numbers[1];
    let m = numbers[2];

    match modexp(x, y, m) {
        Ok(i) => println!("Number is {}", i),
        Err(err) => println!("An error occured {}", err),
    }
}

//! This is the mod exp function.
//! This function takes three numbers as input and returns a Result<u64, &'static str>
//! if y or m are equal to 0, then a Result error is returned.
//! I'm not sure of the math of this function and I'm not that concerned.
//! The algorithm was copied from the wiki pages and it is passing all relevant tests.
fn modexp(x: u64, y: u64, m: u64) -> Result<u64, &'static str> {
    if m == 0 {
        return Err("m cannot equal 0");
    }

    if y == 0 {
        return Err("y cannot equal 0");
    }

    if m == 1 {
        return Ok(0);
    }

    let mut x = x as u128;
    let mut y = y as u128;
    let m = m as u128;

    let mut z = 1;

    while y > 0 {
        if y % 2 == 1 {
            z = (z * x) % m;
        }
        y /= 2;

        //https://docs.rs/num/latest/num/fn.checked_pow.html
        match checked_pow(x, 2) {
            Some(v) => x = v % m,
            None => return Err("x is too big."),
        }
    }

    u64::try_from(z).map_err(|_| "Can't convert z back to u64 result")
}

//! This is the function that runs a series of tests to make sure that the modexp function is correct.
#[test]
fn test_modexp() {
    // Largest prime less than 2**64.
    // https://primes.utm.edu/lists/2small/0bit.html
    let bigm = u64::max_value() - 58;
    assert_eq!(Ok(0), modexp(bigm - 2, bigm - 1, 1));
    assert_eq!(Ok(1), modexp(bigm - 2, bigm - 1, bigm));
    assert_eq!(
        Ok(827419628471527655),
        modexp(bigm - 2, (1 << 32) + 1, bigm)
    );
    // https://practice.geeksforgeeks.org/problems/
    //    modular-exponentiation-for-large-numbers/0
    assert_eq!(Ok(4), modexp(10, 9, 6));
    assert_eq!(Ok(34), modexp(450, 768, 517));
    assert_eq!(Err("m cannot equal 0"), modexp(44, 33, 0));
    assert_eq!(Err("y cannot equal 0"), modexp(0, 0, 517));
}
